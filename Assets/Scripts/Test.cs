using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    public float areaWidth = 2;
    public float areaHeight = 2;

    public float angle = 90;
    public float distance = 1;
    public float offset = 1;
    public Vector2 simplePosition;

    void Start()
    {

    }

    void OnDrawGizmos()
    {

#if UNITY_EDITOR
        Vector3 areaSize = new Vector3(areaWidth, areaHeight, .1f);
        Vector3 areaPos = transform.position + Vector3.up * (offset + (areaHeight / 2f));

        var x = Mathf.Cos(angle * Mathf.Deg2Rad);

        var y = Mathf.Sin(angle * Mathf.Deg2Rad);

        Vector3 finalPos = transform.position + new Vector3(x * distance, y * distance, 0);

        Debug.DrawLine(transform.position, finalPos, Color.green);

        if (finalPos.x > areaPos.x - areaWidth / 2f &&
            finalPos.x < areaPos.x + areaWidth / 2f &&
            finalPos.y > areaPos.y - areaHeight / 2f &&
            finalPos.y < areaPos.y + areaHeight / 2f)
        {
            UnityEditor.Handles.color = Color.green;
            // + new Vector3(areaWidth / 2f,areaHeight / 2f);
            simplePosition = (finalPos - areaPos + new Vector3(areaWidth / 2f, areaHeight / 2f)) / new Vector2(areaWidth, areaHeight);

        }
        else
        {
            UnityEditor.Handles.color = Color.red;

        }

        UnityEditor.Handles.DrawWireDisc(finalPos, Vector3.back, .1f);
        UnityEditor.Handles.color = Color.yellow;

        UnityEditor.Handles.DrawWireCube(areaPos, areaSize);
#endif
    }

}