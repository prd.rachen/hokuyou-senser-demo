namespace D1.Modules.Config.Single
{
    public static partial class DefaultConfig
    {
        public const string SENSOR_IP = "192.168.1.242";
        public const int SENSOR_PORT = 10940;
        public const int START_ANGLE = -45;
        public const int START_RAYCAST = 0;
        public const int END_RAYCAST = 1080;
        public const int MIN_DISTANCE = 1;
        public const int MAX_DISTANCE = 3000;
        public const bool SENSOR_ENDABLE = true;
        public const bool MODE_USB = false;
        public const string SENSOR_PORT_NAME = "11111";
        public const int SENSOR_BAUD_RATE = 11111;
        public const int INPUT_DELAY = 300;
        public const float INPUT_DISTANCE = 0.02f;
        public const float INPUT_LERP_TIME = 0.05f;
        public const bool CURSOR_ENABLE = true;

        public const float SCREEN_HEIGHT = 0.8f;
        public const float SCREEN_OFFSET_X = 0f;
        public const float SCREEN_OFFSET_Y = 0f;

        public const int SCREEN_POS_X = 0;
        public const int SCREEN_POS_Y = 0;
        public const int SCREEN_SIZE_WIDTH = 1920;
        public const int SCREEN_SIZE_HEIGHT = 1080;
        public const int WINDOW_STYLE_MODE = 1;
    }
}