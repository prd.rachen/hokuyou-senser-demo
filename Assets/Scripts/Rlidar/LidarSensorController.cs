using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using D1.Scripts.Sensor.Controllers;
using RPLidar;
using UniRx;
using UnityEngine;

public class LidarSensorController : AbstractSensorController
{
    [Header("Setting")]
    [SerializeField] private string m_PortName = "COM3";
    [SerializeField] private int m_BaudRate = 115200;
    [SerializeField] private ScanMode m_Mode = ScanMode.Legacy;
    [SerializeField] private bool m_IsFlip = true;
    [SerializeField] private FloatReactiveProperty m_AngleOffset = new FloatReactiveProperty(90);

    private readonly Lidar _Lidar = new Lidar("COM3", 115200);
    private CancellationTokenSource _CancellationSource;
    private Task _LidarTask;
    private Scan _Scan;

    void Start()
    {
        _Lidar.PortName = m_PortName;
        _Lidar.Baudrate = m_BaudRate;
        _Lidar.ReceiveTimeout = 3000;
        _Lidar.IsFlipped = m_IsFlip;
        _Lidar.AngleOffset = m_AngleOffset.Value;

        m_AngleOffset.Subscribe(_ => _Lidar.AngleOffset = _).AddTo(this);

        if (_Lidar.Open())
        {
            IsAvailable = _Lidar.IsOpen;
            _CancellationSource = new CancellationTokenSource();
            _LidarTask = Task.Run(() => Scan(m_Mode, _CancellationSource.Token));
            OnInit?.Invoke();
        }

        Observable.EveryUpdate().Where(_x => _Scan != null).Subscribe(_ =>
        {
            for (int i = 0; i < _Scan.Measurements.Count; i++)
            {
                var measurement = _Scan.Measurements[i];

                if (measurement.Distance <= float.Epsilon) continue;

                var x = ((measurement.Distance) * (float)Math.Cos(Math.PI / 180.0f * measurement.Angle));
                var y = ((measurement.Distance) * (float)Math.Sin(Math.PI / 180.0f * measurement.Angle));
                var finalPos = transform.position + new Vector3(x, y, 0);

                if (m_ShowDebug) Debug.DrawLine(transform.position, finalPos, Color.green);
                OnTouch?.Invoke(new TouchData(finalPos, i));
            }
        }).AddTo(this);

    }

    private void Scan(ScanMode mode, CancellationToken cancellationToken)
    {
        // Main loop
        while (!cancellationToken.IsCancellationRequested)
        {
            // Try to start lidar
            if (!StartLidar(mode))
            {
                // Reset and try to start again in a while to avoid high CPU load if something breaks
                _Lidar.Reset();
                Thread.Sleep(1000);
                continue;
            }

            // Run lidar
            if (!RunLidar(cancellationToken))
            {
                // Reset and try to start again
                _Lidar.Reset();
                continue;
            }
        }

        // Stop lidar
        StopLidar();

    }
    private bool StartLidar(ScanMode mode)
    {
        // Get health
        HealthInfo health = _Lidar.GetHealth();
        if (health == null)
        {
            return false;
        }

        // Good health ?
        if (health.Status != HealthStatus.Good)
        {
            Debug.LogWarning($"Health {health.Status}, error code {health.ErrorCode}.");
            return false;
        }

        // Good health
        Debug.Log($"Health good.");

        // Get configuration
        Configuration config = _Lidar.GetConfiguration();
        if (config == null)
        {
            return false;
        }

        // Show configuration
        Debug.Log("Configuration:");
        foreach (KeyValuePair<ushort, ScanModeConfiguration> modeConfig in config.Modes)
        {
            Debug.Log($"0x{modeConfig.Key:X4} - {modeConfig.Value}"
                + (config.Typical == modeConfig.Key ? " (typical)" : string.Empty));
        }

        // Start motor
        _Lidar.ControlMotorDtr(true);

        // Start scanning
        if (!_Lidar.StartScan(mode))
        {
            return false;
        }

        // Report
        Debug.Log("Scanning started.");

        return true;
    }

    /// <summary>
    /// Stop lidar
    /// </summary>
    private void StopLidar()
    {
        // Stop scanning
        _Lidar.StopScan();
        _Lidar.ControlMotorDtr(false);

        // Report
        Debug.Log("Scanning stopped");
    }

    private bool RunLidar(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            // Try to get scan
            Scan scan = _Lidar.GetScan(cancellationToken);
            if (scan == null)
            {
                // It was either cancellation or error
                return cancellationToken.IsCancellationRequested;
            }

            // Display it
            _Scan = scan;
        }

        Debug.Log("Normal exit");
        // Normal exit
        return true;
    }

    void OnDestroy()
    {
        DeInit();
    }
    void OnDisable()
    {
        DeInit();
    }

    void OnApplicationQuit()
    {
        DeInit();
    }

    private void DeInit()
    {
        _CancellationSource?.Cancel();
        _CancellationSource?.Dispose();

        _LidarTask?.GetAwaiter().GetResult();

        _Lidar?.Close();
    }

    public override int GetRayAmount()
    {
        return 360*2;
    }
}
