﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using D1.Modules.Config.Single;

// ReSharper disable All

#if !UNITY_EDITOR && UNITY_STANDALONE_WIN
using System;
using System.IO;
using System.Runtime.InteropServices;
using D1.Modules.Config.Single;
#endif

namespace D1.Scripts.Utilities
{
	public class ResolutionInitializer : MonoBehaviour
	{
		public Camera[] cameras;
		public Camera[] Cameras => ManagedCameras.ToArray();

		public enum DisplayModes { Unknown = -1, Fullscreen = 0, Borderless = 1, Windowed = 2 }

		#region Public Properties
		public DisplayModes CurrentDisplayMode { get { return m_CurrentDisplayMode; } }
		public bool ChangingDisplayMode { get { return m_DisplayModeRoutine != null; } }
		public string CannotTestAllowedResolutionsWarningString { get { return "Warning!! Desktop resolution was not detected. In Windowed and Borderless modes, some resolutions may be too large for your monitor which can lead to rendering and input problems."; } }
		public int TargetX = 0;
		public int TargetY = 0;
		public int TargetWidth = 1920;
		public int TargetHeight = 710;
		public bool IsFullscreen { get { return Screen.fullScreen; } }
		#endregion

		private bool _moveable = false;

		#region Public Methods
		public static bool ManageCamera(Camera camera)
		{
			if (ManagedCameras.Add(camera))
			{
				camera.ResetAspect();
				return true;
			}
			else
			{
				return false;
			}
		}

		public void SetResolution(int x, int y, int width, int height)
		{
			UnityEngine.Debug.LogFormat("Set Resolution: \nX:{0}\nY:{1}\nWidth:{2}\nHeight:{3}", x, y, width, height);
			TryChangeResolution(x, y, width, height);
		}

		public void TestFullscreen()
		{
			TryChangeDisplayMode(DisplayModes.Fullscreen);
		}

		public void TestBorderless()
		{
			TryChangeDisplayMode(DisplayModes.Borderless);
		}

		public void TestWindowed()
		{
			TryChangeDisplayMode(DisplayModes.Windowed);
		}

		public bool IsAllowedResolution(Resolution resolution)
		{
			// if we are windowed or borderless, we need to fit within our desktop resolution
			if (CurrentDisplayMode == ResolutionInitializer.DisplayModes.Windowed || CurrentDisplayMode == ResolutionInitializer.DisplayModes.Borderless)
			{
				// if we can get the desktop resolution
				Vector2? desktopResolution = GetDesktopResolution();
				if (desktopResolution.HasValue)
				{
					// return if the resolution fits
					bool widthFits = resolution.width <= Mathf.CeilToInt(desktopResolution.Value.x);
					bool heightFits = resolution.height <= Mathf.CeilToInt(desktopResolution.Value.y);
					return widthFits && heightFits;
				}
			}

			return true;
		}

		public bool CanTestAllowedResolutions()
		{
			// determine if we are desktop resolution dependent
			bool desktopResolutionDependent = CurrentDisplayMode == ResolutionInitializer.DisplayModes.Windowed || CurrentDisplayMode == ResolutionInitializer.DisplayModes.Borderless;

			// if we are not desktop resolution dependent, we can test allowed resolutions because unity will stretch all resolutions to fit
			if (!desktopResolutionDependent) return true;

			// if we are desktop resolution dependent and we know the resolution, we can test resolutions properly, otherwise we cannot
			return desktopResolutionDependent && GetDesktopResolution().HasValue;
		}

		public Vector2? GetDesktopResolution()
		{
			Vector2? desktopResolution = null;

#if UNITY_STANDALONE_WIN && !UNITY_EDITOR                
			desktopResolution = _windowsHandler.GetDesktopResolution();
#endif

			return desktopResolution;
		}

		public bool TryChangeResolution(int x, int y, int width, int height)
		{
			// not initialized
			if (!m_Initialized) return false;

			// currently changing resolution
			if (m_ResolutionRoutine != null) return false;

			// invalid value
			if (TargetX < 0 || TargetY < 0 || width < 1 || height < 1) return false;

			TargetX = x;
			TargetY = y;
			TargetWidth = width;
			TargetHeight = height;
			m_ResolutionRoutine = ChangeResolutionRoutine();

			StartCoroutine(m_ResolutionRoutine);

			return true;
		}

		public bool TryChangeDisplayMode(DisplayModes desiredDisplayMode)
		{
			// not initialized
			if (!m_Initialized) return false;

			// already at display mode
			if (m_CurrentDisplayMode == desiredDisplayMode) return false;

			// currently changing
			if (m_DisplayModeRoutine != null) return false;

			// start a coroutine to change it
			m_DisplayModeRoutine = ChangeDisplayModeRoutine(desiredDisplayMode);
			StartCoroutine(m_DisplayModeRoutine);

			return true;
		}
		#endregion

		#region Private Variables
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
		private WindowHandler _windowsHandler;
#endif

		private static readonly HashSet<Camera> ManagedCameras = new HashSet<Camera>();
		private DisplayModes m_CurrentDisplayMode = DisplayModes.Unknown;
		private IEnumerator m_DisplayModeRoutine;
		private IEnumerator m_ResolutionRoutine;
		private bool m_Initialized;

		private readonly Var<int> m_ScreenPosX = Var<int>.Define(nameof(DefaultConfig.SCREEN_POS_X), DefaultConfig.SCREEN_POS_X);
		private readonly Var<int> m_ScreenPosY = Var<int>.Define(nameof(DefaultConfig.SCREEN_POS_Y), DefaultConfig.SCREEN_POS_Y);
		private readonly Var<int> m_ScreenPosWidth = Var<int>.Define(nameof(DefaultConfig.SCREEN_SIZE_WIDTH), DefaultConfig.SCREEN_SIZE_WIDTH);
		private readonly Var<int> m_ScreenPosHeight = Var<int>.Define(nameof(DefaultConfig.SCREEN_SIZE_HEIGHT), DefaultConfig.SCREEN_SIZE_HEIGHT);
		private readonly Var<int> m_WindowMode = Var<int>.Define(nameof(DefaultConfig.WINDOW_STYLE_MODE), DefaultConfig.WINDOW_STYLE_MODE);

		#endregion

		#region Private Methods

		protected void Awake()
		{
			QualitySettings.vSyncCount = 0;
			Application.targetFrameRate = 120;

			TargetX = m_ScreenPosX.Value;
			TargetY = m_ScreenPosY.Value;
			TargetWidth = m_ScreenPosWidth.Value;
			TargetHeight = m_ScreenPosHeight.Value;

			Init
				(
					Application.productName,
					TargetX,
					TargetY,
					TargetWidth,
					TargetHeight,
					m_WindowMode.Value.Equals((int) DisplayModes.Fullscreen) ?
					DisplayModes.Fullscreen :
					m_WindowMode.Value.Equals((int) DisplayModes.Borderless) ?
					DisplayModes.Borderless :
					DisplayModes.Windowed
				);
		}

		public void Init(string _name, int _x, int _y, int _width, int _height, DisplayModes _mode)
		{
			TargetX = _x;
			TargetY = _y;
			TargetWidth = _width;
			TargetHeight = _height;

#if !UNITY_EDITOR && UNITY_STANDALONE_WIN

			_windowsHandler = new WindowHandler(_name);

			for (int i = 0; i < cameras.Length; i++)
				ManageCamera(cameras[i]); // Temporary

			InitializeDisplay(_mode);
#endif
		}

		private void InitializeDisplay(DisplayModes _mode)
		{
			// Temporarily selecting display mode based on if we started in full screen, we should use the last used setting instead.
			DisplayModes desiredDisplayMode = IsFullscreen ? DisplayModes.Fullscreen : _mode;

			StartCoroutine(InitializeResolutionAndDisplayModeRoutine(desiredDisplayMode));
		}

		public void Debug()
		{
			UnityEngine.Debug.Log("Init: " + m_Initialized);
		}

		private IEnumerator InitializeResolutionAndDisplayModeRoutine(DisplayModes targetDisplayMode)
		{
			yield return StartCoroutine(ChangeResolutionRoutine());
			yield return StartCoroutine(ChangeDisplayModeRoutine(targetDisplayMode));

			m_Initialized = true;
		}

		private IEnumerator ChangeDisplayModeRoutine(DisplayModes targetDisplayMode)
		{
			// set target display mode as the current display mode
			m_CurrentDisplayMode = targetDisplayMode;

			// set full/windowed, this resets the renderer
			bool rendererReset;
			switch (targetDisplayMode)
			{
				case DisplayModes.Fullscreen:
					rendererReset = Screen.fullScreen != true;
					Screen.fullScreen = true;
					break;

				case DisplayModes.Borderless:
				case DisplayModes.Windowed:
				default:
					rendererReset = Screen.fullScreen != false;
					Screen.fullScreen = false;
					break;
			}

			// wait until the next frame while the renderer resets (http://docs.unity3d.com/ScriptReference/Screen-fullScreen.html)
			if (rendererReset) yield return null;

			// update the window's frame, position, size, etc.
			UpdateWindow();

			// clear the routine handler
			m_DisplayModeRoutine = null;
		}

		private void UpdateWindow()
		{
			// Windows Standalone Player
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
			_windowsHandler.TrySetDisplayMode(m_CurrentDisplayMode, TargetX, TargetY, TargetWidth, TargetHeight);
#endif
		}
		private void UpdateWindowMiddle()
		{
			// Windows Standalone Player
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
			_windowsHandler.TrySetDisplayMode(m_CurrentDisplayMode, TargetWidth, TargetHeight);
#endif
		}

		private IEnumerator ChangeResolutionRoutine()
		{
			// set resolution, if we are full screen this resets the renderer
			bool rendererReset = IsFullscreen;

			// wait until the next frame while the renderer resets (http://docs.unity3d.com/ScriptReference/Screen-fullScreen.html)
			if (rendererReset) yield return null;

			// update window, incase it changed
			UpdateWindow();

			// update camera aspects, incase it changed
			UpdateCameraAspects();

			// clear the routine handler
			m_ResolutionRoutine = null;
		}

		private void Update()
		{
			// Windows Standalone Player
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR

			if (Input.GetMouseButton(0) && _moveable)
				_windowsHandler.OnMouseDown();

#endif
		}

		private void UpdateCameraAspects()
		{
			// set the desired aspect ratio (the values in this example are
			// hard-coded for 16:9, but you could make them into public
			// variables instead so you can set them at design time)
			float targetaspect = 16.0f / 9.0f;

			// determine the game window's current aspect ratio
			float windowaspect = TargetWidth / (float) TargetHeight;

			// current viewport height should be scaled by this amount
			float scaleheight = windowaspect / targetaspect;

			foreach (var camera in ManagedCameras)
			{
				// obtain camera component so we can modify its viewport
				// if scaled height is less than current height, add letterbox
				if (scaleheight < 1.0f)
				{
					Rect rect = camera.rect;

					rect.width = 1.0f;
					rect.height = scaleheight;
					rect.x = 0;
					rect.y = (1.0f - scaleheight) / 2.0f;

					camera.rect = rect;
				}
				else // add pillarbox
				{
					float scalewidth = 1.0f / scaleheight;

					Rect rect = camera.rect;

					rect.width = scalewidth;
					rect.height = 1.0f;
					rect.x = (1.0f - scalewidth) / 2.0f;
					rect.y = 0;

					camera.rect = rect;
				}

				camera.ResetAspect();

			}
		}

		#endregion

		#region Classes
#if UNITY_STANDALONE_WIN && !UNITY_EDITOR
		public class WindowHandler
		{
			[StructLayout(LayoutKind.Sequential)]
			public struct RECT
			{
				public int Left; // x position of upper-left corner
				public int Top; // y position of upper-left corner
				public int Right; // x position of lower-right corner
				public int Bottom; // y position of lower-right corner
			}

			public const int WM_NCLBUTTONDOWN = 0xA1;
			public const int HT_CAPTION = 0x2;

			// import methods
			[DllImport("user32.dll", EntryPoint = "SetWindowLong", SetLastError = true, CharSet = CharSet.Auto)]
			public static extern IntPtr SetWindowLongPtr(IntPtr hWnd, int nIndex, int dwNewLong);

			[DllImport("user32.dll", EntryPoint = "GetWindowLong", SetLastError = true, CharSet = CharSet.Auto)]
			public static extern IntPtr GetWindowLongPtr(IntPtr hWnd, int nIndex);

			[DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
			public static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

			[DllImport("user32.dll", EntryPoint = "GetDesktopWindow", SetLastError = true)]
			public static extern IntPtr GetDesktopWindow();

			[DllImport("user32.dll", EntryPoint = "SetWindowPos", SetLastError = true)]
			public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int uFlags);

			[DllImport("user32.dll", EntryPoint = "GetWindowRect", SetLastError = true)]
			public static extern bool GetWindowRect(IntPtr hWnd, out RECT rect);

			[DllImport("user32.dll", EntryPoint = "GetClientRect", SetLastError = true)]
			public static extern bool GetClientRect(IntPtr hWnd, out RECT rect);

			[DllImportAttribute("user32.dll")]
			public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

			[DllImportAttribute("user32.dll")]
			public static extern bool ReleaseCapture();

			// constructor
			public WindowHandler(string title)
			{
				_title = title;
			}

			public Vector2 GetDesktopResolution()
			{
				RECT desktopRect;
				GetWindowRect(Desktop, out desktopRect);

				return new Vector2(desktopRect.Right - desktopRect.Left, desktopRect.Bottom - desktopRect.Top);
			}

			public void OnMouseDown()
			{
				try
				{
					ReleaseCapture();
					SendMessage(Window, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
				}
				catch
				{ }
			}

			public bool TrySetDisplayMode(DisplayModes targetDisplayMode, int resolutionWidth, int resolutionHeight)
			{
				// setup
				int flags = (int) GetWindowLongPtr(Window, GWL_STYLE);

				// desktop rect
				RECT desktopRect;
				GetWindowRect(Desktop, out desktopRect);
				int desktopWidth = desktopRect.Right - desktopRect.Left;
				int desktopHeight = desktopRect.Bottom - desktopRect.Top;

				switch (targetDisplayMode)
				{
					// fullscreen
					case DisplayModes.Fullscreen:
						return true;

						// borderless
					case DisplayModes.Borderless:
						// FIRST PASS: positions the client window correctly
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowRect(Window, (desktopWidth - resolutionWidth) / 2, (desktopHeight - resolutionHeight) / 2, resolutionWidth, resolutionHeight);

						// SECOND PASS: ensures that the window has the correct styling
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						//UpdateWindowStyle(Window);                    // for some reason, UpdateWindowStyle does not update the window 
						// properly here, and instead resets of the window styles. For 
						// some other reason, a secondary call to SetWindowLongPtr does 
						// in fact update the window properly. It is not clear why this 
						// is, only that it seems to work, for now, in our test environment.                    
						SetWindowLongPtr(Window, GWL_STYLE, flags);

						return true;

						// windowed
					case DisplayModes.Windowed:
						// FIRST PASS: determine how many pixels are needed to render the window decorations on each side (top, bottom, left, right)
						Flags.Set<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowStyle(Window);

						// window and client rects
						RECT windowRect, clientRect;
						GetWindowRect(Window, out windowRect);
						GetClientRect(Window, out clientRect);

						// calculate decoration size                    
						int decorationWidth = (windowRect.Right - windowRect.Left) - (clientRect.Right - clientRect.Left);
						int decorationHeight = (windowRect.Bottom - windowRect.Top) - (clientRect.Bottom - clientRect.Top);

						// SECOND PASS: position the client window correctly, w.r.t. decorations
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowRect(Window, (desktopWidth - resolutionWidth - decorationWidth) / 2, (desktopHeight - resolutionHeight - decorationHeight) / 2, resolutionWidth + decorationWidth, resolutionHeight + decorationHeight);

						// THIRD PASS: ensures that the window has the correct styling
						Flags.Set<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowStyle(Window);

						return true;

						// other
					case DisplayModes.Unknown:
					default:
						return false;
				}
			}

			public bool TrySetDisplayMode(DisplayModes targetDisplayMode, int x, int y, int resolutionWidth, int resolutionHeight)
			{
				// setup
				int flags = (int) GetWindowLongPtr(Window, GWL_STYLE);

				// desktop rect
				RECT desktopRect;
				GetWindowRect(Desktop, out desktopRect);
				int desktopWidth = desktopRect.Right - desktopRect.Left;
				int desktopHeight = desktopRect.Bottom - desktopRect.Top;

				switch (targetDisplayMode)
				{
					// fullscreen
					case DisplayModes.Fullscreen:
						return true;

						// borderless
					case DisplayModes.Borderless:
						// FIRST PASS: positions the client window correctly
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowRect(Window, x, y, resolutionWidth, resolutionHeight);

						// SECOND PASS: ensures that the window has the correct styling
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						//UpdateWindowStyle(Window);                    // for some reason, UpdateWindowStyle does not update the window 
						// properly here, and instead resets of the window styles. For 
						// some other reason, a secondary call to SetWindowLongPtr does 
						// in fact update the window properly. It is not clear why this 
						// is, only that it seems to work, for now, in our test environment.                    
						SetWindowLongPtr(Window, GWL_STYLE, flags);

						return true;

						// windowed
					case DisplayModes.Windowed:
						// FIRST PASS: determine how many pixels are needed to render the window decorations on each side (top, bottom, left, right)
						Flags.Set<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowStyle(Window);

						// window and client rects
						RECT windowRect, clientRect;
						GetWindowRect(Window, out windowRect);
						GetClientRect(Window, out clientRect);

						// calculate decoration size                    
						int decorationWidth = (windowRect.Right - windowRect.Left) - (clientRect.Right - clientRect.Left);
						int decorationHeight = (windowRect.Bottom - windowRect.Top) - (clientRect.Bottom - clientRect.Top);

						// SECOND PASS: position the client window correctly, w.r.t. decorations
						Flags.Unset<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowRect(Window, x, y, resolutionWidth + decorationWidth, resolutionHeight + decorationHeight);

						// THIRD PASS: ensures that the window has the correct styling
						Flags.Set<int>(ref flags, WS_CAPTION);
						SetWindowLongPtr(Window, GWL_STYLE, flags);
						UpdateWindowStyle(Window);

						return true;

						// other
					case DisplayModes.Unknown:
					default:
						return false;
				}
			}

			private IntPtr Window { get { return FindWindowByCaption(IntPtr.Zero, _title); } }
			private IntPtr Desktop { get { return GetDesktopWindow(); } }

			private void UpdateWindowStyle(IntPtr window)
			{
				SetWindowPos(window, 0, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOOWNERZORDER | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
			}

			private void UpdateWindowRect(IntPtr window, int x, int y, int width, int height)
			{
				SetWindowPos(window, -2, x, y, width, height, SWP_FRAMECHANGED);
			}

			private bool TestForErrors(IntPtr result)
			{
				if (result == IntPtr.Zero)
				{
					int errorCode = Marshal.GetLastWin32Error();
					if (errorCode != 0)
					{
						UnityEngine.Debug.LogError("Error " + errorCode.ToString() + " occured. SetDisplayMode failed.");
						return true;
					}
				}

				return false;
			}

			// style flags
			private const int
			WS_BORDER = 0x00800000,
				WS_CAPTION = 0x00C00000,
				WS_CHILD = 0x40000000,
				WS_CHILDWINDOW = 0x40000000,
				WS_CLIPCHILDREN = 0x02000000,
				WS_CLIPSIBLINGS = 0x04000000,
				WS_DISABLED = 0x08000000,
				WS_DLGFRAME = 0x00400000,
				WS_GROUP = 0x00020000,
				WS_HSCROLL = 0x00100000,
				WS_ICONIC = 0x20000000,
				WS_MAXIMIZE = 0x01000000,
				WS_MAXIMIZEBOX = 0x00010000,
				WS_MINIMIZE = 0x20000000,
				WS_MINIMIZEBOX = 0x00020000,
				WS_OVERLAPPED = 0x00000000,
				WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
				WS_POPUP = unchecked((int) 0x80000000),
				WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
				WS_SIZEBOX = 0x00040000,
				WS_SYSMENU = 0x00080000,
				WS_TABSTOP = 0x00010000,
				WS_THICKFRAME = 0x00040000,
				WS_TILED = 0x00000000,
				WS_TILEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
				WS_VISIBLE = 0x10000000,
				WS_VSCROLL = 0x00200000;

			// extended style flags
			private const int
			WS_EX_DLGMODALFRAME = 0x00000001,
				WS_EX_CLIENTEDGE = 0x00000200,
				WS_EX_STATICEDGE = 0x00020000;

			// position flags
			private const int
			SWP_FRAMECHANGED = 0x0020,
				SWP_NOMOVE = 0x0002,
				SWP_NOSIZE = 0x0001,
				SWP_NOZORDER = 0x0004,
				SWP_NOOWNERZORDER = 0x0200,
				SWP_SHOWWINDOW = 0x0040,
				SWP_NOSENDCHANGING = 0x0400;

			// index for style and extended style flag management
			private const int
			GWL_STYLE = -16,
				GWL_EXSTYLE = -20;

			private string _title;
		}
#endif
		#endregion
	}
}