namespace D1.Modules.Config.Multiple
{
    public static partial class DefaultMultipleConfig
    {
        //--------------------------------------------------------------------------------------------------------------

        private const int _SETTINGS_INSTANCES_COUNT = 0;

        //--------------------------------------------------------------------------------------------------------------

        public static int GetInstanceCount() => _SETTINGS_INSTANCES_COUNT;

        //--------------------------------------------------------------------------------------------------------------
    }
}